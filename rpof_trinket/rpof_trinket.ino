/*
 * eRPOf trinket
 * TheFwGuy - September 2018
 * This code control the power ON/OFF on a Raspberry Pi
 * PB0 -> input  - pushbutton
 * PB1 -> output - pushbutton LED
 * PB2 -> output - enable
 * PB3 -> output - shutdown command
 * PB4 -> input  - shutdown feedback
 */

/* Main state machine defines */

#define STEADY_OFF  0
#define WAIT_ON     1
#define STEADY_ON   2
#define WAIT_OFF    3
#define LAST_DELAY  4

/* LED state machine defines */

#define LED_OFF         0
#define LED_FLASH_SLOW  1
#define LED_ON          2
#define LED_FLASH_FAST  3

#define WATCHDOG_TIME   20000     // 20 seconds watchdog
#define SHUTDOWN_TIME   7000      // 7 seconds delay before to power off 
#define LED_TIME_FAST   60        // 60 ms
#define LED_TIME_SLOW   110       // 110 ms
/* Global variables */

int mainState;
int btnRead;
int feedbackRead;
int ledState;
int ledToggle;

int ledDelay;
int shutdownDelay;
int watchdogDelay;   // Watchdog variable used to escape loop after 20 seconds

// Update the counters if > than 0 - wait ~1 ms
void updateDelays()
{
  if (ledDelay > 0) ledDelay--;
  if (shutdownDelay > 0) shutdownDelay--;
  if (watchdogDelay > 0) watchdogDelay--;

  // Wait 1 ms only if at least one counter is on
  if (ledDelay > 0 || shutdownDelay > 0 || watchdogDelay > 0) delay(1);  
}

void resetDelays()
{
  ledDelay      = 0;
  watchdogDelay = 0;
  shutdownDelay = 0;  
}

bool isDelayExpired(int counter)
{
   return counter > 0 ? false : true;
}

// Turn the LED ON or OFF every time is called
void ledFlash()
{
   digitalWrite(PB1, (ledToggle ? HIGH : LOW));
   ledToggle = ledToggle ? 0 : 1;
}

// Handle LED
void ledStateMachine() 
{
   switch(ledState) {
      default:
      case LED_OFF:
         digitalWrite(PB1, LOW);  // turn pushbutton LED OFF
         break;

      case LED_FLASH_SLOW:
      case LED_FLASH_FAST:
         if (isDelayExpired(ledDelay))
         {
            ledDelay = (ledState == LED_FLASH_SLOW) ? LED_TIME_SLOW : LED_TIME_FAST;  // Assign delay value for LED flash
            ledFlash();
         }
         break;
         
      case LED_ON:
         digitalWrite(PB1, HIGH);  // turn pushbutton LED ON
         break;
   }
}

// Read pushbutton with debounce and invert logic
// Don't call this function from the main loop but only when needed, it
// introduces delays

void readBtn()
{
   btnRead = LOW;
   if (digitalRead(PB0) == HIGH) 
      return; 

   delay(5);    // Wait 5m
   // Is still low ?

   if (digitalRead(PB0) == HIGH) 
      return; 

   // Yes --- waiting to go back high
   
   while (digitalRead(PB0) == LOW);
   delay(1);
   btnRead = HIGH;
   return;
}

// Read shutdown feedback from Raspberry
// Don't call this function from the main loop but only when needed, it
// introduces delays

void readShutdownFeedback()
{
   feedbackRead = HIGH;
   if (digitalRead(PB4) == HIGH) 
      return; 

   delay(5);    // Wait 5m
   // Is still low ?

   if (digitalRead(PB4) == HIGH) 
      return; 

   feedbackRead = LOW;
   return;
}

// Set states

void setOFF()
{
   digitalWrite(PB2, HIGH);  // disable power
   digitalWrite(PB3, HIGH);  // NO shutdwon
   ledState     = LED_OFF;
   mainState    = STEADY_OFF;
   feedbackRead = HIGH;
   resetDelays();
}

// Main state machine
void mainStateMachine()
{
   switch (mainState) 
   {
      default:
      case STEADY_OFF:
         readBtn();
         if (btnRead == HIGH)
         {
            digitalWrite(PB2, LOW);   // enable power
            digitalWrite(PB3, HIGH);  // NO shutdwon

            ledState      = LED_FLASH_SLOW;
            mainState     = WAIT_ON;
            watchdogDelay = WATCHDOG_TIME;
          }
          break;

      case WAIT_ON:
          readShutdownFeedback();
          
          if (isDelayExpired(watchdogDelay))
             feedbackRead = HIGH;            
         
         if (feedbackRead == HIGH)
         {
            ledState = LED_ON;
            mainState = STEADY_ON;
            delay(50);
            resetDelays();
          } 
          break;
       
      case STEADY_ON:
         readBtn();
         if (btnRead == HIGH)
         {
            digitalWrite(PB3, LOW);   // Start shutdwon
            ledState      = LED_FLASH_FAST;
            watchdogDelay = WATCHDOG_TIME;
            mainState     = WAIT_OFF;
         }
         break;

      case WAIT_OFF:
         readShutdownFeedback();
         
         if (isDelayExpired(watchdogDelay))
            setOFF();

         if (feedbackRead == LOW)
         {
            ledState      = LED_FLASH_FAST;
            shutdownDelay = SHUTDOWN_TIME;
            watchdogDelay = WATCHDOG_TIME;
            mainState     = LAST_DELAY;
         }
         break;

      case LAST_DELAY:
         if (isDelayExpired(shutdownDelay) || isDelayExpired(watchdogDelay))
            setOFF();
         break;   
   }
}

void setup() 
{
  pinMode(PB0, INPUT);  // Pushbutton - active LOW
  pinMode(PB1, OUTPUT); // Pushbutton LED - active HIGH
  pinMode(PB2, OUTPUT); // Enable PowerBoost - power when LOW
  pinMode(PB3, OUTPUT); // Shutdown command - active LOW
  pinMode(PB4, INPUT);  // Shutdown feedback - HIGH indicates Raspberry running

  digitalWrite(PB1, LOW);  // turn pushbutton LED OFF
  digitalWrite(PB2, HIGH);  // disable power
  digitalWrite(PB3, HIGH);  // Disable shutdown

  btnRead       = LOW;
  feedbackRead  = HIGH;
  ledState      = LED_OFF;
  mainState     = STEADY_OFF;

  resetDelays();
}

void loop() 
{
   updateDelays();
   mainStateMachine();
   ledStateMachine();
}

